#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* reverse(const char* str) {
  
  int i;
  int length = strlen(str);
  char * reversed_string = (char*) malloc(length+1);

  for(i = 0; i < length; ++i)
  {
    reversed_string[i] = str[(length-1) - i];
  }
  reversed_string[length] = '\0';

  return reversed_string;
}
